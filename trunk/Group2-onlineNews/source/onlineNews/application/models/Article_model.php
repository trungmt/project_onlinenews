<?php
 
class Article_model extends CI_Model
{
    
    //khong co dong nay la 
	// khong chay duoc
    public function __construct(){
	parent:: __construct();
	$this->load->database();
    }
           
    //ham nay load tat ca article
    function getArticles()
    {
        $query = $this->db->get('article');
        return $query->result();
    }
    
    //ham nay duoc goi trong file Home.php
    //load article gan day nhat gioi han la 3
    function getLimitedArticle($id){
        $this->db->select('article.id, title, content, thumbnail');
        $this->db->from('article');
        $this->db->where('topic_id',$id);
        $this->db->order_by('create', 'DESC');
        $this->db->limit(3);
        $query = $this->db->get();
        if ($query->num_rows() > 1) {
            return $query->result();
        } else {
            return false;
        }
    }
    
    //ham nay duoc goi trong file Article_star.php
    //load tat ca article thuoc chu de Star
    function getAllStarArticle($id){
        $this->db->select('*');
        $this->db->from('article');
        $this->db->where('topic_id',$id);
        $this->db->order_by('create', 'DESC');
        $query = $this->db->get();
        if ($query->num_rows() > 1) {
            return $query->result();
        } else {
            return false;
        }
    }
    
    //ham sap xep tin tuc giam dan theo ngay thang nam
    function sortDesc($id){
        $this->db->select('*');
        $this->db->from('article');
        $this->db->where('topic_id',$id);
        $this->db->order_by('create', 'DESC');
        $query = $this->db->get();
        if ($query->num_rows() > 1) {
            return $query->result();
        } else {
            return false;
        }
    }
    
    //ham sap xep tin tuc tang dan theo ngay thang nam
    function sortAsc($id){
        $this->db->select('*');
        $this->db->from('article');
        $this->db->where('topic_id',$id);
        $this->db->order_by('create', 'ASC');
        $query = $this->db->get();
        if ($query->num_rows() > 1) {
            return $query->result();
        } else {
            return false;
        }
    }
    
    
    //ham de tim kiem bai viet theo keyword
    function findMatchedArticle($keyword){
            $this->db->select('*');
            $this->db->from('article');
            $this->db->order_by('create','DESC');
            $this->db->like('article.title', $keyword); 
            $query = $this->db->get();
            if($query -> num_rows() >= 1)
                {
                 return $query->result();
                }
            else
                {
                 return null;
                }
    }
    
    //khi click vao tung bai bao de doc thi se goi ham nay
    //de hien thi noi dung chi tiet cua bai bao
    function getDetailedArticle($id){
        $this->db->select('*');
        $this->db->from('article');
        $this->db->where('article.id', $id);
        $query = $this->db->get();
        if ($query->num_rows() >= 1) {
            return $query->row();
        } else {
            return false;
        }
    }
}

?>