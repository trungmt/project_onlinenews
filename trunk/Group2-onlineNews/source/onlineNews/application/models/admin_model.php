<?php
class admin_model extends CI_Model {
    function __construct()
        {
            parent::__construct();
            $this->load->database();
        }
    function getDetailedAllArticle(){
        $this->db->select('*');
        $this->db->from('article');
        $query = $this->db->get();
        if ($query->num_rows() >= 1) {
            return $query->result();
        } else {
            return false;
        }
    }
    function getDetailedArticle($id){
        
        $this->db->select('*');
        $this->db->from('article');
        $this->db->where('id',$id);
        $query = $this->db->get();
        if ($query->num_rows() >= 1) {
            return $query->row();
        } else {
            return false;
        }
    }
    function editArtical($id,$data){
        
        $this->db->where('id',$id);
        $this->db->set('topic_id',$data['topic_id']);
        $this->db->set('title',$data['title']);
        $this->db->set('content',$data['content']);
        $this->db->set('thumbnail',$data['thumbnail']);
        $this->db->set('create',$data['create']);
        $this->db->update('article');
        
    }
    function delArticle($id){
        $this->db->where('article.id', $id);
        $this->db->delete('article');
    }
    function addArticle($data){
        $this->db->set('topic_id',$data['topic_id']);
        $this->db->set('title',$data['title']);
        $this->db->set('content',$data['content']);
        $this->db->set('thumbnail',$data['thumbnail']);
        $this->db->set('create',$data['create']);
        $this->db->insert('article');
    }
}            
  
