<?php
class Admin extends CI_Controller {
    public function __construct(){
	parent::__construct();
        $this->load->library('session');
        $this->load->helper('form');
        $user_data = $this->session->userdata('newdata');
        if(empty($user_data)){
        $this->load->view('Login_View');
        }
        
    }
    function index()
    {
        $this->load->model('admin_model');
        $data['artical_detail'] = $this->admin_model->getDetailedAllArticle();
        $user_data = $this->session->userdata('newdata');
        if(!empty($user_data)){
            $data['user'] = $user_data;
        }
        $this->load->view('Admin_View',$data);
    }
    //load all artical detail
    function editArtical(){
        $id = $_GET['id'];
        $this->load->model('admin_model');
        $data['artical_detail'] = $this->admin_model->getDetailedArticle($id);
        $user_data = $this->session->userdata('newdata');
        if(!empty($user_data)){
            $data['user'] = $user_data;
        }
        
        $this->load->view('Edit_Artical_view',$data);
    }
    function doEditArticle(){
        $string = "begin logging";
        $id = $this->input->post('id'); 
        $topic_id = $this->input->post('group'); 
        $title = $this->input->post('title');
        $content = $this->input->post('content');
        $thumbnail = $this->input->post('thumbnail');
        $create = $this->input->post('create');
        
        $file = fopen("C:\\text.txt",'w');
        $string .= "id= ". $id . " content = " . $content. " ";
       
        $data_array = array (
            
            'id' => $id,
            'topic_id' => $topic_id,
            'title' =>   $title,
            'content' =>$content,
            'thumbnail' => $thumbnail,
            'create' => $create
                );
        $this->load->model('admin_model'); 
        //$query = true;
        $query = $this->admin_model->editArtical($id,$data_array);
        $string .= $query;
        fwrite($file,$string); 
        redirect('admin','refresh');
          
    }
    function delNew($news_id){
        $this->load->model('admin_model');
        $this->admin_model->delArticle($news_id);
        // $this->load->view('Home_view',$data);
        redirect('admin','refresh');
    }
    function showAddNewsPage(){
        $user_data = $this->session->userdata('newdata');
        if(empty($user_data)){
           
        }else {
            $data['user'] = $user_data;
            $this->load->view('Insert_Artical_view',$data);
        }
        
    }
    function addNews(){
        $this->load->model('admin_model');
        
        
        $topic_id = $this->input->post('group'); 
        $title = $this->input->post('title');
        $content = $this->input->post('content');
        $thumbnail = $this->input->post('thumbnail');
        $create = $this->input->post('create');
        
        $data_array = array (
            
            'topic_id' => $topic_id,
            'title' =>   $title,
            'content' =>$content,
            'thumbnail' => $thumbnail,
            'create' => $create
                );
        $this->admin_model->addArticle($data_array);
        redirect('admin','refresh');
    }
}