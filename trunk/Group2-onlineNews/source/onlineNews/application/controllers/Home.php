<?php
class Home extends CI_Controller {

    public function __construct(){
	parent::__construct();
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->helper('form');
    }
    
    
    function index()
    {
        $this->load->model('Article_model');
        //load toi da 3 bai bao ve chu de star luu vao bien article_star
        //so 1 truyen vao la topic_id = 1 la thuoc ve topic star trong CSDL
        $data['article_star'] = $this->Article_model->getLimitedArticle(1);
        //load toi da 3 bai bao ve health luu vao bien article_health
        //so 1 truyen vao la topic_id = 2 la thuoc ve topic health tron CSDL
        $data['article_health'] = $this->Article_model->getLimitedArticle(2);
        //load toi da 3 bai bao ve health luu vao bien article_entertain
        //so 1 truyen vao la topic_id = 3 la thuoc ve topic entertain tron CSDL
        $data['article_entertain'] = $this->Article_model->getLimitedArticle(3);
        $user_data = $this->session->userdata('newdata');
        if(isset($user_data)){
            $data['user'] = $user_data;
        }
        $this->load->view('Home_view',$data);
    }
     
    //khi muon doc noi dung chi tiet 1 tin tuc nao do
    //ta click vao tin tuc do
    //se hien thi trang Article_view_detailed de hien thi noi dung chi tiet tin tuc do
    function readNew($news_id){
        $this->load->model('Article_model');
        $data['article_detailed'] = $this->Article_model->getDetailedArticle($news_id);
        $this->load->view('Article_view_detailed',$data);
    }
}
?>