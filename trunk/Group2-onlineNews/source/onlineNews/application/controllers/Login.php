<?php
class Login extends CI_Controller {
    private $newdata = array();
    public function __construct(){
	parent::__construct();
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->helper('form');
        
    }
    function index()
    {
        $user_data = $this->session->userdata('newdata');
        if(empty($user_data)){
        $this->load->view('Login_View');
        }
        else {
            redirect('home','refresh');
        }
    }
    function login(){
       
        $this->form_validation->set_rules('username','Username','required');
        $this->form_validation->set_rules('password','Password','required|min_length[6]|max_length[100]');
        if ($this->form_validation->run() == FALSE)
            {
              $this->load->view('Login_View');
            }                    
        else{
            
              $username = $this->input->post('username'); 
              $password = $this->input->post('password');
              $hash = sha1($password);
              $this->load->model('login_model'); 
              $query = $this->login_model->login($username,$hash);
              if ($query)
                {
                 foreach($query as $row)
                 {
                     $newdata = array(
                            'id' => $row->id,
                            'username'   => $row->username
                        );
                     $this->session->set_userdata('newdata',$newdata); // Tạo Session cho Users                 
                     redirect('home','refresh'); // Đăng nhập thành công chuyển bạn về trang home.html
                 }
             return TRUE;
                }
              else
                {
                    $msg=array('login_error'=>'Wrong user/pass')   ;                           
                    $this->load->view('Login_View',$msg); 
                 return false;   
                }             
        }
        
    }
    function logout() {
        
        $this->session->unset_userdata('newdata');
        $this->session->set_flashdata('flash_message', 'Đăng xuất thành công');
        redirect('home','refresh');
    }
    function changePassword(){
        $user_data = $this->session->userdata('newdata');
        if(empty($user_data)){
           
        }else {
            $data['user'] = $user_data;
            $this->load->view('change_password_view',$data);
        }
    }
    function doChangePassword(){
        $check = false;
        $old_password = $this->input->post('old_password'); 
        $hash = sha1($old_password);
        $new_password = $this->input->post('new_password');
        $return_new_password = $this->input->post('return_new_password');
        
        $user_data = $this->session->userdata('newdata');
        $username = $user_data['username'];
        $this->load->model('login_model'); 
        $query1 = $this->login_model->login($username,$hash);
        if($query1){
            if($new_password != $return_new_password){
                $data['change_error'] = "Your confirm-password is not match";
            }else{
                $query = $this->login_model->changePassword($username,  sha1($new_password));
                $check = true;
                }
            }
        else{
                $data['change_error'] = "Your old-password is not match";
        }
        if($check == true){
                redirect('home','refresh');
        }else{
            if(!empty($user_data)){
                $data['user'] = $user_data;
            }
            $this->load->view('change_password_view',$data);
        }
    }
}
 
?>