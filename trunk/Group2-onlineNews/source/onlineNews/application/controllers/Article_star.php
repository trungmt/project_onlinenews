<?php
class Article_star extends CI_Controller {

    public function __construct(){
	parent::__construct();
    }
    
    function index($id)
    {
        $this->load->model('Article_model');
        //load tat ca nhung bai bao ve chu de 
        ///star luu vao bien article_star_all
        //so 1 truyen vao la topic_id = 1 la thuoc ve topic star trong CSDL
        $data['article_all_star'] = $this->Article_model->getAllStarArticle($id);
        $data['specify_id'] = $id;
        $this->load->view('Article_star_view',$data);
    }
    
    //sort tang dan ngay thang nam
    function sortAsc($id)
    {
        $this->load->model('Article_model');
        //load tat ca nhung bai bao ve chu de star luu vao bien article_star_all
        //so 1 truyen vao la topic_id = 1 la thuoc ve topic star trong CSDL
        $data['article_all_star'] = $this->Article_model->sortAsc($id);
        $data['specify_id'] = $id;
        $this->load->view('Article_star_view',$data);
    }
     
    
    //sort giam dan ngay thang nam
    function sortDesc($id)
    {
        $this->load->model('Article_model');
        //load tat ca nhung bai bao ve chu de star luu vao bien article_star_all
        //so 1 truyen vao la topic_id = 1 la thuoc ve topic star trong CSDL
        $data['article_all_star'] = $this->Article_model->sortDesc($id);
        $data['specify_id'] = $id;
        $this->load->view('Article_star_view',$data);
    }
    
    //ham de tim kiem bai bao theo keyword
    function findArticle($key)
    {
        $this->load->model('Article_model');
        //load tat ca nhung bai bao ve chu de star luu vao bien article_star_all
        //so 1 truyen vao la topic_id = 1 la thuoc ve topic star trong CSDL
        $query = $this->Article_model->findMatchedArticle($key);
        //ta se goi ham de tim kiem trong CSDL nhung article co title trung voi keyword nhap vao
        //neu co trung thi hien thi tat ca article len
        //tren trang Article_find_view
        //neu khong tim thay thi hien thi trang Article_not_find_view
        if($query!= null)
        {
            $data['article_find_all'] = $this->Article_model->findMatchedArticle($key);
            $this->load->view('Article_find_view',$data);
        }
        else
        {
            $data_not_find['error_not_find'] = 'Không tìm thấy dữ liệu cần tìm.';
            $this->load->view('Article_not_find_view',$data_not_find);
        }
    }
}
?>