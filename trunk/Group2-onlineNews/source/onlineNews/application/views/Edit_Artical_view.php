<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
    <meta name="author" content="GeeksLabs">
    <link rel="shortcut icon" href="img/favicon.png">
    <title>OnlineNews</title>   
    
    <link rel="stylesheet" href="<?php echo base_url();?>public/css/bootstrap.min.css">
    <!-- bootstrap theme -->
    <link rel="stylesheet" href="<?php echo base_url();?>public/css/bootstrap-theme.css">
    <!--external css-->
    <!-- font icon -->
    <link rel="stylesheet" href="<?php echo base_url();?>public/css/elegant-icons-style.css">
    <link rel="stylesheet" href="<?php echo base_url();?>public/css/font-awesome.css">
    <!-- Custom styles -->
    <link rel="stylesheet" href="<?php echo base_url();?>public/css/style1.css">
   
  </head>

  <body>
  <!-- container section start -->
  <section id="container" class="">
     
      
      <header class="header dark-bg">
            <div class="toggle-nav">
                
            </div>

            <!--logo start-->
            <a href="<?php echo base_url()."/index.php/home/"?>" class="logo">Online <span class="lite">News</span></a>
            <!--logo end -->

            <div class="top-nav notification-row">                
                    <li class="dropdown" style ="list-style: none;">
                        <a href="<?php echo base_url()."/index.php/admin/"?>">
                            <span class="profile-ava">
                                <img alt="" src="<?php echo base_url();?>/public/images/avatar1_small.jpg">
                            </span>
                            <span class="username">
                                <?php echo $user['username'];?>
                            </span>
                            <b class="caret"></b>
                        </a>
<!--                        <ul class="dropdown-menu extended logout">
                            <div class="log-arrow-up"></div>
                            <li class="eborder-top">
                                <a href="#"><i class="icon_profile"></i> My Profile</a>
                            </li>
                            <li>
                                <a href="#"><i class="icon_chat_alt"></i>Password</a>
                            </li>
                            <li>
                                <a href="#"><i class="icon_key_alt"></i> Log Out</a>
                            </li>
                        </ul>-->
                    </li>
                    <!-- user login dropdown end -->
                </ul>
                <!-- notificatoin dropdown end-->
            </div>
      </header>      
      <!--header end-->

      <!--sidebar start-->
      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu">       
                   <li class="active    ">
                      <a class="" href="<?php echo base_url()."/index.php/admin/"?>">
                          <i class="glyphicon glyphicon-repeat"></i>
                          <span><?php echo $user['username'] ?> </span>
                      </a>
                  </li>
                  <li class="active">
                      <a class="" href="<?php echo base_url()."/index.php/home/"?>">
                          <i class="glyphicon glyphicon-home"></i>
                          <span>HOME PAGE</span>
                      </a>
                  </li>
                  <li class="active">
                      <a class="" href="<?php echo base_url()."/index.php/admin/showAddNewsPage/"?>">
                          <i class="glyphicon glyphicon-plus"></i>
                          <span>THÊM BÀI</span>
                      </a>
                  </li>
                  <li class="active">
                      <a class="" href="<?php echo base_url()."/index.php/login/changePassword/"?>">
                          <i class="glyphicon glyphicon-lock"></i>
                          <span>PASSWORD</span>
                      </a>
                  </li>
                  <li class="active">
                      <a class="" href="<?php echo base_url()."/index.php/login/logout/"?>">
                          <i class="glyphicon glyphicon-arrow-left"></i>
                          <span>ĐĂNG XUẤT</span>
                      </a>
                  </li>
                  
              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->
      
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">            
              <!--overview start-->
            <div class="row">
		<div class="col-lg-12">
                    <h3 class="page-header"><i class="glyphicon glyphicon-eye-open"></i><strong>ADMIN <?php echo $user['username']."'S";?> PAGE</strong></h3>
		</div>
            </div>
              <!-- project team & activity end -->
               <!--Password start-->
                    <div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                              <h3><i class="glyphicon glyphicon-pencil"></i><strong>Chỉnh sửa bài báo</strong></h3>
                          </header>
                          <div class="panel-body">
                              <div class="form">
                                  <div class="form">
                                       <form class="form-validate form-horizontal" id="feedback_form" method="post" action="<?php echo base_url()."index.php/Admin/doEditArticle/"?>">
                                              <div class="form-group ">
                                                    <label for="id" class="control-label col-lg-2">ID<span class="required"></span></label>
                                                    <div class="col-lg-10">
                                                        <input class="form-control" id="id" name="id" value="<?php echo $artical_detail->id ?>"   type="text" required readonly />
                                                     </div>
                                              </div>
                                              <div class="form-group ">
                                                    <label for="id" class="control-label col-lg-2">Chủ đề<span class="required">*</span></label>
                                                    <div class="col-lg-10">
                                                        <input class="form-control" id="id" name="group" value="<?php echo $artical_detail->topic_id ?>"   type="text" required />
                                                     </div>
                                              </div>
                                              <div class="form-group ">
                                                    <label for="tieude" class="control-label col-lg-2">Tiêu đề <span class="required">*</span></label>
                                                    <div class="col-lg-10">
                                                         <input class="form-control" id="title" name="title" value="<?php echo $artical_detail->title ?>"   type="text" required />
                                                     </div>
                                              </div>
                                                  <div class="form-group">
                                                        <label class="control-label col-sm-2">Nội dung</label>
                                                        <div class="col-sm-10">
                                                             <textarea class="form-control ckeditor" name="content" rows="6"><?php echo $artical_detail->content ?></textarea>
                                                         </div>
                                                     </div>
                                                <div class="form-group ">
                                                    <label for="hinhanh" class="control-label col-lg-2">Hình ảnh<span class="required">*</span></label>
                                                    <div class="col-lg-10">
                                                        <input class="form-control" id="thumbnail" name="thumbnail" value="<?php echo $artical_detail->thumbnail ?>"  maxlength="100" type="text" required />
                                                     </div>
                                                </div>
                                                <div class="form-group ">
                                                    <label for="date" class="control-label col-lg-2">Ngày đăng<span class="required">*</span></label>
                                                    <div class="col-lg-10">
                                                         <input class="form-control" id="create" name="create" value="<?php echo $artical_detail->create ?>"  type="text" required />
                                                     </div>
                                                </div>
                                                      <div class="form-group">
                                          <div class="col-lg-6" ></div>
                                          <div class="col-lg-6">
                                              <button class="btn btn-primary" type="submit">Edit</button>
<!--                                              <button class="btn btn-default" type="button">Cancel</button>-->
                                          </div>
                                      </div>
                                       </form>
                                      </div>
                              </div>
                              </div>
                      </section>
                  </div>
              </div>
              <!--Password end-->

          </section>
      </section>
      <!--main content end-->
  </section>
    <script src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/ckeditor/ckeditor.js"></script> <!-- editor-->

  </body>
</html>
