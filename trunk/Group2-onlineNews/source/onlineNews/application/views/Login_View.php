<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
    <meta name="author" content="GeeksLabs">
    <meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
    <link rel="shortcut icon" href="img/favicon.png">
    
    <title>Online News_Login Page</title>
    <!-- Bootstrap CSS -->    
    <link rel="stylesheet" href="<?php echo base_url();?>public/css/bootstrap.min.css">
    <!-- bootstrap theme -->
    <link rel="stylesheet" href="<?php echo base_url();?>public/css/bootstrap-theme.css">
    <!--external css-->
    <!-- font icon -->
    <link rel="stylesheet" href="<?php echo base_url();?>public/css/elegant-icons-style.css">
    <link rel="stylesheet" href="<?php echo base_url();?>public/css/font-awesome.css">
    <!-- Custom styles -->
    <link rel="stylesheet" href="<?php echo base_url();?>public/css/style1.css">
    
</head>

<body class="login-img3-body" >
    

    <div class="container">

      <form class="login-form" action="<?php echo base_url()."index.php/login/login/" ?>" method="post">        
        <div class="login-wrap">
            <p class="login-img"><i class="glyphicon glyphicon-send"></i></p>
            <div class="input-group">
              <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
              <input name ="username" type="text" class="form-control" placeholder="Username" autofocus>
              <span class="error"><?php echo form_error('username'); ?></span>
            </div>
            <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-ok"></i></span>
                <input name ="password" type="password" class="form-control" placeholder="Password">
                <span class="error"><?php echo form_error('password'); ?></span>
            </div>
            
            <button class="btn btn-primary btn-lg btn-block" type="submit">Login</button>
            <p class="p_error" style="color: red"><?php if(isset($login_error)) { echo $login_error;} ?></p>
        </div>
      </form>
        
        

    </div>
    

  </body>
</html>
