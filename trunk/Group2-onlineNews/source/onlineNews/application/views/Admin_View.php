
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
    <meta name="author" content="GeeksLabs">
    <link rel="shortcut icon" href="img/favicon.png">
    <title>OnlineNews</title>   
    <link rel="stylesheet" href="<?php echo base_url();?>public/css/bootstrap.min.css">
    <!-- bootstrap theme -->
    <link rel="stylesheet" href="<?php echo base_url();?>public/css/bootstrap-theme.css">
    <!--external css-->
    <!-- font icon -->
    <link rel="stylesheet" href="<?php echo base_url();?>public/css/elegant-icons-style.css">
    <link rel="stylesheet" href="<?php echo base_url();?>public/css/font-awesome.css">
    <!-- Custom styles -->
    <link rel="stylesheet" href="<?php echo base_url();?>public/css/style1.css">
   
  </head>

  <body>
  <!-- container section start -->
  <section id="container" class="">
     
      
      <header class="header dark-bg">
            <div class="toggle-nav">
                
            </div>

            <!--logo start-->
            <a href="<?php echo base_url()."/index.php/home/"?>" class="logo">Online <span class="lite">News</span></a>
            <!--logo end -->

            <div class="top-nav notification-row">                
                    <li class="dropdown" style ="list-style: none;">
                        <a href="<?php echo base_url()."/index.php/admin/"?>">
                            <span class="profile-ava">
                                <img alt="" src="<?php echo base_url();?>/public/images/avatar1_small.jpg">
                            </span>
                            <span class="username">
                                <?php echo $user['username'];?>
                            </span>
                            <b class="caret"></b>
                        </a>
<!--                        <ul class="dropdown-menu extended logout">
                            <div class="log-arrow-up"></div>
                            <li class="eborder-top">
                                <a href="#"><i class="icon_profile"></i> My Profile</a>
                            </li>
                            <li>
                                <a href="#"><i class="icon_chat_alt"></i>Password</a>
                            </li>
                            <li>
                                <a href="#"><i class="icon_key_alt"></i> Log Out</a>
                            </li>
                        </ul>-->
                    </li>
                    <!-- user login dropdown end -->
                </ul>
                <!-- notificatoin dropdown end-->
            </div>
      </header>      
      <!--header end-->

      <!--sidebar start-->
      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu">       
                   <li class="active    ">
                      <a class="" href="<?php echo base_url()."/index.php/admin/"?>">
                          <i class="glyphicon glyphicon-repeat"></i>
                          <span><?php echo $user['username'] ?> </span>
                      </a>
                  </li>
                  <li class="active">
                      <a class="" href="<?php echo base_url()."/index.php/home/"?>">
                          <i class="glyphicon glyphicon-home"></i>
                          <span>HOME PAGE</span>
                      </a>
                  </li>
                  <li class="active">
                      <a class="" href="<?php echo base_url()."/index.php/admin/showAddNewsPage/"?>">
                          <i class="glyphicon glyphicon-plus"></i>
                          <span>THÊM BÀI</span>
                      </a>
                  </li>
                  <li class="active">
                      <a class="" href="<?php echo base_url()."/index.php/login/changePassword/"?>">
                          <i class="glyphicon glyphicon-lock"></i>
                          <span>PASSWORD</span>
                      </a>
                  </li>
                  <li class="active">
                      <a class="" href="<?php echo base_url()."/index.php/login/logout/"?>">
                          <i class="glyphicon glyphicon-arrow-left"></i>
                          <span>ĐĂNG XUẤT</span>
                      </a>
                  </li>
                  
              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->
      
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">            
              <!--overview start-->
			  <div class="row">
				<div class="col-lg-12">
                                    <h3 class="page-header"><i class="glyphicon glyphicon-eye-open"></i><strong>ADMIN <?php echo $user['username']."'S";?> PAGE</strong></h3>
                                    <a href="<?php echo base_url()."/index.php/Admin/showAddNewsPage/";?>"><button class="btn btn-info" type="button"><i class="glyphicon glyphicon-plus"></i>  Add  </button></a>
				
				</div>
                          </div>
                    <div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <header style="text-align: center" class="panel-heading">
                              <i class="glyphicon glyphicon-list"></i> BÀI BÁO
                          </header>
                          <table class="table table-striped table-advance table-hover">
                           <tbody>
                              <tr>
                                 <th><i class="glyphicon glyphicon-bell"></i> ID</th>
                                 <th><i class="glyphicon glyphicon-folder-open"></i>  GROUP</th>
                                 <th><i class="glyphicon glyphicon-paperclip"></i> Tiêu đề</th>    
                                 <th><i class="glyphicon glyphicon-comment"></i> Nội dung</th>
                                 <th><i class="glyphicon glyphicon-picture"></i> Hình ảnh</th>
                                 <th><i class="glyphicon glyphicon-calendar"></i> Ngày đăng</th>
                                 <th><i class="glyphicon glyphicon-cog"></i> Chức năng</th>
                                 
                              </tr>
                        <?php
                            
                            foreach($artical_detail as $p)
                            {

                                if (strlen($p->content) > 60) {
                                    $content = substr($p->content, 0, 60);
                                    $p->content = substr($content, 0, strrpos($content, ' ')).'...';
                                }
                            
                         ?>  

<!--                              <tr>
                                 <td>Angeline Mcclain</td>
                                 <td>2004-07-06</td>
                                 <td>dale@chief.info</td>
                                 <td>Rosser</td>
                                 <td>176-026-5992</td>
                                 <td>
                                  <div class="btn-group">
                                      <a class="btn btn-success" href="#"><i class="icon_plus_alt2"></i></a>
                                      <a class="btn btn-danger" href="#"><i class="icon_close_alt2"></i></a>
                                  </div>
                                  </td>
                              </tr>-->
                              <tr>
                                 <td class="col-lg-1"><?php echo $p->id?></td>
                                 <td class="col-lg-1"><?php echo $p->topic_id?></td>
                                 <td class="col-lg-2"><?php echo $p->title?></td>
                                 <td class="col-lg-3"><?php echo $p->content?></td>
                                 <td class="col-lg-1"><?php echo $p->thumbnail?></td>
                                 <td class="col-lg-2"><?php echo date ("d/m/Y",strtotime($p->create));  ?></td>
                                 <td class="col-lg-2">
                                  <div class="btn-group">
                                      <a class="btn btn-success col-lg-6" href="<?php echo base_url()."index.php/Admin/editArtical?id=".$p->id."/" ?>"><i class="glyphicon glyphicon-pencil"></i></a>
                                      <a class="btn btn-danger col-lg-6" href="<?php echo base_url()."/index.php/Admin/delNew/".$p->id;?>"><i class="glyphicon glyphicon-remove"></i></a>
                                  </div>
                                  </td>
                              </tr>
                              <?php 
                            
                                } ?>
                           </tbody>
                        </table>
                      </section>
                  </div>
              </div>
              <!--Password end-->

          </section>
      </section>
      <!--main content end-->
  </section>
    <script src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/ckeditor/ckeditor.js"></script> <!-- editor-->

  </body>
</html>
