<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
    <meta name="author" content="GeeksLabs">
    <meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
    <link rel="shortcut icon" href="img/favicon.png">

    <title>OnlineNews</title>

    <link rel="stylesheet" href="<?php echo base_url();?>public/css/bootstrap.min.css">
    <!-- bootstrap theme -->
    <link rel="stylesheet" href="<?php echo base_url();?>public/css/bootstrap-theme.css">
    <!--external css-->
    <!-- font icon -->
    <link rel="stylesheet" href="<?php echo base_url();?>public/css/elegant-icons-style.css">
    <link rel="stylesheet" href="<?php echo base_url();?>public/css/font-awesome.css">
    <!-- Custom styles -->
    <link rel="stylesheet" href="<?php echo base_url();?>public/css/style1.css">
   
  </head>

  <body>
  <!-- container section start -->
  <section id="container" class="">
     
      
      <header class="header dark-bg">
            <div class="toggle-nav">
                
            </div>

            <!--logo start-->
            <a href="<?php echo base_url()."/index.php/home/"?>" class="logo">Online <span class="lite">News</span></a>
            <!--logo end -->

            <div class="top-nav notification-row">                
                    <li class="dropdown" style ="list-style: none;">
                        <a href="<?php echo base_url()."/index.php/admin/"?>">
                            <span class="profile-ava">
                                <img alt="" src="<?php echo base_url();?>/public/images/avatar1_small.jpg">
                            </span>
                            <span class="username">
                                <?php echo $user['username'];?>
                            </span>
                            <b class="caret"></b>
                        </a>
<!--                        <ul class="dropdown-menu extended logout">
                            <div class="log-arrow-up"></div>
                            <li class="eborder-top">
                                <a href="#"><i class="icon_profile"></i> My Profile</a>
                            </li>
                            <li>
                                <a href="#"><i class="icon_chat_alt"></i>Password</a>
                            </li>
                            <li>
                                <a href="#"><i class="icon_key_alt"></i> Log Out</a>
                            </li>
                        </ul>-->
                    </li>
                    <!-- user login dropdown end -->
                </ul>
                <!-- notificatoin dropdown end-->
            </div>
      </header>      
      <!--header end-->

      <!--sidebar start-->
      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu">       
                   <li class="active    ">
                      <a class="" href="<?php echo base_url()."/index.php/admin/"?>">
                          <i class="glyphicon glyphicon-repeat"></i>
                          <span><?php echo $user['username'] ?> </span>
                      </a>
                  </li>
                  <li class="active">
                      <a class="" href="<?php echo base_url()."/index.php/home/"?>">
                          <i class="glyphicon glyphicon-home"></i>
                          <span>HOME PAGE</span>
                      </a>
                  </li>
                  <li class="active">
                      <a class="" href="<?php echo base_url()."/index.php/admin/showAddNewsPage/"?>">
                          <i class="glyphicon glyphicon-plus"></i>
                          <span>THÊM BÀI</span>
                      </a>
                  </li>
                  <li class="active">
                      <a class="" href="<?php echo base_url()."/index.php/login/changePassword/"?>">
                          <i class="glyphicon glyphicon-lock"></i>
                          <span>PASSWORD</span>
                      </a>
                  </li>
                  <li class="active">
                      <a class="" href="<?php echo base_url()."/index.php/login/logout/"?>">
                          <i class="glyphicon glyphicon-arrow-left"></i>
                          <span>ĐĂNG XUẤT</span>
                      </a>
                  </li>
                  
              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->
      
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">            
              <!--overview start-->
                <div class="row">
                    <div class="col-lg-12">
                        <h3 class="page-header"><i class="glyphicon glyphicon-eye-open"></i><strong>ADMIN <?php echo $user['username']."'S";?> PAGE</strong></h3>
                    </div>
                </div>
              <!-- project team & activity end -->
               <!--Password start-->
                    <div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                              <h3><i class="glyphicon glyphicon-lock"></i><strong>Change password</strong></h3>
                          </header>
                          <div class="panel-body">
                              <div class="form">
                                  <form class="form-validate form-horizontal" id="feedback_form" method="post" action="<?php echo base_url()."index.php/login/doChangePassword/" ?>">
                                      <div class="form-group ">
                                          <label for="oldpass" class="control-label col-lg-2">Mật khẩu cũ <span class="required">*</span></label>
                                          <div class="col-lg-10">
                                              <input class="form-control" id="oldpass" name="old_password" minlength="5" type="password" required />
                                          </div>
                                      </div>
                                      <div class="form-group ">
                                          <label for="newpass" class="control-label col-lg-2">Mật khẩu mới <span class="required">*</span></label>
                                          <div class="col-lg-10">
                                              <input class="form-control " id="newpass"  name="new_password" type="password" required />
                                          </div>
                                      </div>
                                       <div class="form-group ">
                                          <label for="retpass" class="control-label col-lg-2">Nhập lại mật khẩu <span class="required">*</span></label>
                                          <div class="col-lg-10">
                                              <input class="form-control " id="retpass"  name="return_new_password" type="password" required />
                                          </div>
                                      </div>
                                      <div class="form-group">
                                          <div class="col-lg-6" ></div>
                                          <div class="col-lg-6">
                                              <button class="btn btn-primary" type="submit">Save</button>
                                              <form action="<?php echo base_url()."index.php/home/" ?>" method="post">
                                                  <button class="btn btn-default" type="button">Cancel</button>
                                              </form>
                                              
                                          </div>
                                          <p class="p_error" style="color: red"><?php if(isset($change_error)) { echo $change_error;} ?></p>
                                      </div>
                                  </form>
                              </div>

                          </div>
                      </section>
                  </div>
              </div>
              <!--Password end-->

          </section>
      </section>
      <!--main content end-->
  </section>
  <!-- container section start -->

    <!-- javascripts -->
    <script src="js/jquery.js"></script>
	<script src="js/jquery-ui-1.10.4.min.js"></script>
    <script src="js/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui-1.9.2.custom.min.js"></script>
    <!-- bootstrap -->
    <script src="js/bootstrap.min.js"></script>
    <!-- nice scroll -->
    <script src="js/jquery.scrollTo.min.js"></script>
    <script src="js/jquery.nicescroll.js" type="text/javascript"></script>
    <!-- charts scripts -->
    <script src="assets/jquery-knob/js/jquery.knob.js"></script>
    <script src="js/jquery.sparkline.js" type="text/javascript"></script>
    <script src="assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
    <script src="js/owl.carousel.js" ></script>
    <!-- jQuery full calendar -->
    <<script src="js/fullcalendar.min.js"></script> <!-- Full Google Calendar - Calendar -->
	<script src="assets/fullcalendar/fullcalendar/fullcalendar.js"></script>
    <!--script for this page only-->
    <script src="js/calendar-custom.js"></script>
	<script src="js/jquery.rateit.min.js"></script>
    <!-- custom select -->
    <script src="js/jquery.customSelect.min.js" ></script>
	<script src="assets/chart-master/Chart.js"></script>
   
    <!--custome script for all page-->
    <script src="js/scripts.js"></script>
    <!-- custom script for this page-->
    <script src="js/sparkline-chart.js"></script>
    <script src="js/easy-pie-chart.js"></script>
	<script src="js/jquery-jvectormap-1.2.2.min.js"></script>
	<script src="js/jquery-jvectormap-world-mill-en.js"></script>
	<script src="js/xcharts.min.js"></script>
	<script src="js/jquery.autosize.min.js"></script>
	<script src="js/jquery.placeholder.min.js"></script>
	<script src="js/gdp-data.js"></script>	
	<script src="js/morris.min.js"></script>
	<script src="js/sparklines.js"></script>	
	<script src="js/charts.js"></script>
	<script src="js/jquery.slimscroll.min.js"></script>


  </body>
</html>
