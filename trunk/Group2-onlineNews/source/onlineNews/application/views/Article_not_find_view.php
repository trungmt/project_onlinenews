<!DOCTYPE html>
<html>
<head>
	<title> Trang danh sach bai bao theo chu de </title>
	
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="<?php echo base_url();?>public/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url();?>public/css/new.css">
	<link rel="stylesheet" href="<?php echo base_url();?>public/css/style.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <script>
            function trackChange(value){
                    window.open("<?php echo base_url().'/index.php/Article_star/findArticle/';?>"+value)
                }
        </script>
</head>
<body>
	<div class="container-fluid">

		<div class="header-container">
			<img id="logopage" src="<?php echo base_url();?>public/images/logopage.png">
			<img id="logo-description" src="<?php echo base_url();?>public/images/logo-description.png">
			<div id="head-login">
				<a href="#"> 
					<span class="glyphicon glyphicon-lock"></span> Đăng nhập
				</a>
			</div>
			
			<div id="head-list-logo">
				<ul class="list-logo">
					<li>
						<img class="image-list-logo" src="<?php echo base_url();?>public/images/info.png">
					</li>
					<li>
						<img class="image-list-logo" src="<?php echo base_url();?>public/images/email.png">
					</li>
				</ul>
			</div>
			
			<div class="clear: right">
			</div>
			
		</div>

		<!--Navigation bar-->
		<nav class="navbar navbar-default1">
		  <div class="container-fluid">
		    <div class="navbar-header">
		      <a class="navbar-brand" href="#">onlineNews</a>
		    </div>
		    <ul class="nav navbar-nav">
		      <li class="active"><a href="<?php echo base_url().'/index.php/Home';?>">Trang chủ</a></li>
		      <li><a href="<?php echo base_url().'/index.php/Article_star/index/1';?>">Ngôi sao</a></li>
		      <li><a href="<?php echo base_url().'/index.php/Article_star/index/2';?>">Sức khỏe</a></li>
		      <li><a href="<?php echo base_url().'/index.php/Article_star/index/3';?>">Giải trí</a></li>
		    </ul>
		    <form class="navbar-form navbar-right" role="search">
        <div class="form-group">
          <input class="form-control" placeholder="Search" type="text" id="myInput">
        </div>
        <button type="submit" class="btn btn-default" onclick="trackChange(myInput.value)"><span class="glyphicon glyphicon-search"></span></button>
      </form>
		    <!--<ul class="nav navbar-nav navbar-right">
		      <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
		    </ul>-->
		  </div>
		</nav>

		<!--container de chua slide dong-->
		<div class="container">

		</div>

		<div class="container">

			<!--danh sach cac bai bao-->
			<div class="row" >
				<!--cot ben trai chua danh sach cac bai bao theo chu de
				cot ben phai chua thanh menu
				theo ti le 9-3-->
				<div class="col-sm-9">
					<div class="alert alert-danger">
						<strong>
							Thông báo!
						</strong>
						<?php echo $error_not_find; ?>
					</div>
				</div>
				<!-- ***********************
				Thanh menu ben phai 
				**************************-->
				<div class="col-sm-3 right-content">
					<div class="panel panel-default right-content-wrap">
						<!--<div class="panel-heading">
							Tin tuc moi nhat
						</div>
						<div class="panel-body" style="margin-left: -10px;">
							<a href="#">Bai bao moi nhat ngay hom nay</a><br>
							<a href="#">Bai bao moi nhat ngay hom nay</a><br>
							<a href="#">Bai bao moi nhat ngay hom nay</a><br>
							<a href="#">Bai bao moi nhat ngay hom nay</a><br>
							<a href="#">Bai bao moi nhat ngay hom nay</a><br>
						</div>-->
						<div class="panel-heading">
							Nhà tài trợ
						</div>
						<div class="panel-body" id="sponsor-wrap">
							<img class="sponsor-image" src="<?php echo base_url();?>public/images/sponsor1.png"/>
							<img class="sponsor-image" src="<?php echo base_url();?>public/images/sponsor2.png"/>
							<img class="sponsor-image" src="<?php echo base_url();?>public/images/sponsor3.png"/>
						</div>
						<div class="panel-body" id="advertise-wrap">
							<div id="first-advertise">
								<img src="<?php echo base_url();?>public/images/img-advertise5.jpg"/>
							</div>
							<div id="second-advertise">
								<img src="<?php echo base_url();?>public/images/img-advertise5.jpg"/>
							</div>
						</div>
					</div>
					<table class="table">
					</table>
				</div>
			</div>
		</div>
		<div id="footer">
			<div class="container">
				<ul id="footer_logo">
					<img src="<?php echo base_url();?>public/images/logo.jpg">
					<h1> Online news </h1>
				</ul>
				<ul id="footer_collaborate">
					<h4> Hợp tác nội dung </h4>
					<span class="glyphicon glyphicon-phone-alt"> 1900 1560 </span>
					<span class="glyphicon glyphicon-envelope"> marketing_news@gmail.com </span>
					<br><br>
					<h4> Liên hệ quảng cáo </h4>
					<span class="glyphicon glyphicon-earphone"> 0168 000 9999 </span>
					<span class="glyphicon glyphicon-envelope"> advertising_news@gmail.com </span>
					<br><br>
					<h4> Chính sách bảo mật </h4>
				</ul>
				<ul id="footer_about">
					<h3> Công ty cổ phần Online news </h3>
					<p> Lý Thường Kiệt, Quận 10, Thành phố Hồ Chí Minh </p>
					<p> Giấy phép số 00000 do Sở thông tin và truyền thông cấp 2016 </p>
				</ul>
			</div>
		</div>
	</div> 
</body>
</html>