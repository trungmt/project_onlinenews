-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 11, 2016 at 06:42 PM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `online_news`
--

-- --------------------------------------------------------

--
-- Table structure for table `article`
--

CREATE TABLE IF NOT EXISTS `article` (
  `id` int(255) NOT NULL,
  `topic_id` int(11) NOT NULL,
  `title` mediumtext COLLATE utf8_unicode_ci,
  `content` mediumtext COLLATE utf8_unicode_ci,
  `thumbnail` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `create` date DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `article`
--

INSERT INTO `article` (`id`, `topic_id`, `title`, `content`, `thumbnail`, `create`) VALUES
(1, 1, 'Toàn bộ video của "Bitches In Town" bất ngờ bị Youtube gỡ bỏ', 'Tài khoản "Bitches In Town" - nơi đăng tải talkshow bị nhận xét là đá xéo, công kích nghệ sĩ Việt gây tranh cãi trong thời gian qua đã bất ngờ bị trang Youtube gỡ bỏ vì vi phạm một số điều khoản về quyền sử dụng.\r\n\r\nThời gian qua, những lùm xùm xoay quanh việc nhiều nghệ sĩ lên tiếng phản đối talkshow “Bitches In Town - Những kẻ lắm lời” được dư luận dành nhiều quan tâm. Nhiều làn sóng trái chiều đưa ra ý kiến đả kích những người sản xuất bởi họ cho rằng tính chất của talkshow này không phù hợp với văn hóa của Việt Nam, đồng thời những lời lẽ họ sử dụng trong show để nói những người nổi tiếng, có sức ảnh hưởng lớn đa phần mang ý bỡn cợt, thô tục.\r\n\r\nMới đây, tài khoản “Bitches In Town” trên trang Youtube trực tiếp đăng tải show này đã bất ngờ bị gỡ bỏ. Điều này đồng nghĩa với việc 22 tập “Những kẻ lắm lời” trước đó, 1 tập “Những kẻ ít lời” mới nhất, cùng những clip liên quan đều bị xóa sổ khỏi trang video lớn nhất thế giới này.\r\nTrước đó, khi vấp phải những công kích của dư luận, bộ ba show này có lên tiếng xin lỗi đến nghệ sĩ về những điều họ còn sai sót trong quá trình thực hiện tập đó. Tuy nhiên, khi nhận chỉ thị chính thức từ Cục Phát thanh, Truyền hình và Thông tin điện tử, Bộ Thông tin và Truyền thông yêu cầu ngưng hoạt động và gỡ bỏ các tập có nội dung phản cảm, bộ ba này vẫn tuyên bố sẽ không ngừng làm chương trình.\r\n\r\n\r\nNgày 3/12 - tròn hai tuầu sau khi tuyên bố tạm nghỉ ngơi, ê-kíp này quyết định trở lại với talkshow này bằng một tên gọi và hình thức khá đi đôi chút - "Những kẻ ít lời". Trong đoạn giới thiệu, MC Thùy Minh tuyên bố bên cạnh tên gọi mới, show này sẽ phát triển với hình thức mới là "ngồi xuống cùng bình luận". Sau đó, họ nêu lên một số chuyên mục chính sẽ phát triển trong show với tên gọi khác đi nhiều so với phiên bản cũ. Tuy nhiên về tính chất, nhiều người xem xong số đầu tiên vẫn nhận thấy rằng họ vẫn đang đi theo hướng "đào sâu" chuyện nghệ sĩ Việt, dù đã có sự tiết chế thay vì công kích mạnh mẽ trước đó.', 'public/images/thumbnails/thumbnail1.jpg', '2016-01-20'),
(2, 1, 'Miu Lê: "Giấy khai sinh của tôi là giới tính nam trong suốt 14 năm"', 'Diễn viên “Em là bà nội của anh” không giấu khỏi bật cười nhiều lần khi chia sẻ về bí mật tuổi thơ cùng những câu chuyện “tưng tửng” của mình.\r\nSau vai diễn cô Thanh Nga trong bộ phim “Em là bà nội của anh”, Miu Lê nhận được nhiều sự quan tâm từ khán giả và giới chuyên môn về khả năng hóa thân vào nhân vật khá “duyên”. Tác phẩm điện ảnh chuyển thể từ kịch bản gốc của Hàn Quốc đã giúp nữ ca sĩ – diễn viên tuổi 24 tiến thêm một bước dài trong sự nghiệp nghệ thuật, sau thời gian dài yên ắng. Thành công này được nhiều người đánh giá là toàn vẹn ở hai lĩnh vực mà Miu Lê đang theo đuổi - âm nhạc và điện ảnh. Bởi bên cạnh lối diễn ấn tượng, cô còn gây bất ngờ bởi khả năng thể hiện những bản nhạc Trịnh một cách nhập tâm và “có hồn”.\r\n\r\nMiu Lê không muốn buổi phỏng vấn một cách nghiêm túc, xoay quanh về sự nghiệp như cô vẫn hay trả lời trước đây. Lần này, cô muốn mang đến người hâm mộ một hình ảnh trẻ trung, vui vẻ và có phần “tưng tửng”. Nữ ca sĩ bắt đầu nói về tính cách thật của mình ngoài đời, qua cảm nhận của những người thân trong gia đình.\r\n\r\n“Tôi khá giống con trai”. Đằng sau câu nói đơn giản này là cả một câu chuyện khá hài hước về “hành trình lấy lại danh phận con gái” của mình sau 14 năm kể từ ngày sinh ra. Thời mẹ mang thai tôi, công nghệ chưa tiên tiến nên không thể siêu âm xác định được giới tính chính xác mà dựa vào mức độ “quậy phá” trong bụng, các thành viên trong gia đình cho rằng tôi sẽ là một bé trai. Mọi người còn đặt sẵn tên và làm khai sinh cho tôi là Lê Anh Nhật. Vậy là đến khi chào đời, tôi đành mang “danh phận” là một cậu nhóc. Mãi đến năm lớp 9, tôi mới được đi sửa tên trong giấy khai sinh thành Lê Ánh Nhật và giới tính là nữ.\r\n\r\nMiu Lê: Giấy khai sinh của tôi là giới tính nam trong suốt 14 năm - Ảnh 2.\r\nCó một câu chuyện tôi chưa từng kể ai đó là nỗi sợ ma và kiến. Ma thì chưa bao giờ gặp nhưng cứ bị ám ảnh vì coi phim kinh dị quá nhiều. Còn nỗi sợ kiến có thể mọi người mới nghe qua sẽ thấy rất vớ vẩn nhưng đối với riêng tôi đây là cả câu chuyện khiếp đảm. Theo lời mẹ kể thì lúc tôi còn nhỏ, chỉ mới biết đi chập chững thì có một lần bị giẫm phải ổ kiến lửa lớn trong một lần chơi ở sân. Từ trong nhà, mẹ nghe tiếng la toáng và khóc lên thì mới phát hiện cả hai cẳng chân tôi đều bị kiến cắn sưng đỏ và mưng mủ đầy chân. Những vết cắn này để lại sẹo đến nhiều năm sau. Gia đình phải bôi thuốc liên tục mới có thể chữa lành cho tôi.\r\n\r\nĐến khi trưởng thành, đây trở thành nỗi sợ lớn nhất và cơ thể như có cơ chế phản ứng xấu với kiến, chỉ cần một vết cắn nhỏ thì xung quanh chân sẽ bị sưng tấy cả một vùng. Tôi còn nghĩ chắc độc tố do kiến cắn lúc bé nhiều quá nên mới bị dị ứng nặng đến mức này. Nhiều người mới tiếp xúc không biết lại nghĩ Miu Lê làm quá vì đi đâu cũng thấy tôi hay xịt thuốc ngừa côn trùng cẩn thận, nhưng sự thật đây là nỗi ám ảnh lớn nhất đời tôi.\r\n\r\nMiu Lê: Giấy khai sinh của tôi là giới tính nam trong suốt 14 năm - Ảnh 3.\r\nLúc đi học, tôi thấy mình cũng khá “men” vì chỉ chơi với con trai thôi, ít có kiểu ngồi tụ tập lại ăn bánh tráng trộn hay ngồi học bài, chơi banh đũa… mà toàn tham gia vào những trò thô bạo như rượt đuổi, đánh nhau, bắn bi thôi. Tôi nghĩ nếu là con trai một ngày, mình sẽ làm những điều mà con gái hay sợ nhất đó là xấu. Tôi sẽ dũng cảm đi nắng mà không phải che chắn găng tay, khẩu trang, đồng thời cũng thử nghiệm phong cách mình thích ở con trai đó là quần jean, áo phông trắng kết hợp giày thể thao.\r\n\r\nTôi thấy tính cách mình không “bình thường” lắm đâu. Nhiều người nghĩ Miu Lê ngoài đời chắc hiền lành, nết na lắm, nhưng phải sống cùng nhà mới biết được bản tính thật của tôi “trên trời” thế nào. Thực ra một phần suy nghĩ tôi cũng vô tư lắm. Khi rảnh rỗi tôi thường làm những chuyện “ruồi bu” linh tinh mà bản thân cảm thấy rất vui nhưng toàn bị người lớn la thôi. Nhiều lần mẹ tôi còn bảo là thấy tôi quá khùng rồi, chắc phải đưa vào bệnh viện tâm thần khám thôi. Từ “khùng” ở đây không phải mắng yêu hay chọc ghẹo mà tôi nghĩ… khùng thiệt.\r\n\r\nMiu Lê: Giấy khai sinh của tôi là giới tính nam trong suốt 14 năm - Ảnh 4.\r\nTôi cũng được nhiều người hỏi rằng “khùng như như vậy có sợ bị ế không?”, nhưng không, tôi thấy đây cũng là nét đặc biệt riêng của mình mà. Nhiều cô gái xung quanh tôi hiện nay cũng có cá tính mạnh và hài hước. Họ cũng được nhiều người khác chú ý tới đấy thôi. Quan trọng là nếu trong một chừng mực nhất định, bạn sẽ biết nhũng điều khác biệt thành cái duyên của mình. Tôi nghĩ nếu mình là con trai thì cũng thích mẫu bạn gái “lạ lạ” như vậy.\r\n\r\nTôi mong muốn kết hôn vào năm 29 tuổi nhưng cũng có thể sớm hơn, chứ đợi 30 thì hơi già rồi. Tôi không đặt quá nhiều tiêu chí để chọn bạn trai, chỉ quan trọng tính cách người đó phải thật tốt và hợp với tôi. Tôi nghĩ tình yêu sẽ thăng hoa ở sự hài hòa trong tính cách. Hai người có thể là những cá thể không tốt khi đứng riêng, nhưng khi kết hợp cùng nhau, họ sẽ bổ trợ cho nhau để trở thành sự hoàn hảo.\r\n', 'public/images/thumbnails/thumbnail2.jpg', '2016-01-21'),
(3, 1, 'Trấn Thành khẳng định không liên quan đến việc Hari Won chia tay Tiến Đạt', 'Nam diễn viên - MC Trấn Thành không muốn lên tiếng về sự việc này và cho rằng nên hỏi người trong cuộc nếu muốn tìm hiểu rõ sự việc.\r\nSáng 18/1, Hari Won gây nhiều bất ngờ cho người hâm mộ khi tuyên bố chia tay Đinh Tiến Đạt sau 9 năm yêu nhau. Theo lời nữ ca sĩ - diễn viên cho biết cô và bạn trai gặp nhiều trục trặc trong vấn đề tình cảm trong một năm qua. Trước đó, cả hai cũng từng chia sẻ có ý định ra mắt hai họ và nghĩ đến chuyện cưới xin.\r\n\r\nTrong bài phỏng vấn, Hari Won cho biết, cô và người yêu cũ đã trải qua 3 năm tìm hiểu nhau. Trước khi kết hôn, nữ ca sĩ đã yêu cầu cả hai phải trả lời thẳng thắn về những điều không thích về nhau. Tuy nhiên, sau nhiều năm quan sát, Hari vẫn không nhận thấy được sự thay đổi của đối phương. Nữ ca sĩ cũng thừa nhận, điều cô không thích ở Tiến Đạt không phải là do tính cách hay nhân phẩm vì với cô, Đinh Tiến Đạt là người đàn ông quá tốt.\r\n\r\nNhiều khán giả sau khi đọc tin đặt ra nhiều nghi vấn về mối quan hệ của cặp đôi "ông già - bà già" của showbiz bị người thứ ba xen giữa. Một số người còn giả thiết rằng Trấn Thành chính là nhân vật này vì căn cứ vào độ thân thiết của Hari Won và nam MC qua những nụ hôn, cái ôm, lời khen ngọt ngào... trong các chương trình thực tế như "Ơn giời, cậu đây rồi", "Hoán đổi"...', 'public/images/thumbnails/thumbnail3.jpg', '2016-01-22'),
(4, 2, 'Thay thế thực phẩm "thông minh" giúp bạn vừa giảm béo vừa khỏe mạnh', 'Cùng một loại thực phẩm có thể cung cấp lượng calo tương đương. Vậy sao bạn không thay ngay những món ăn nhiều đường, gây béo bằng rau xanh nhỉ?\r\nNhững ai có mong muốn giảm cân đều biết, càng giảm lượng calo thì bạn càng hạn chế được cân nặng. Thế nhưng, giảm calo không đồng nghĩa với việc bạn phải ăn ít đi. Có rất nhiều thực phẩm chứa ít calo khiến bạn có thể ăn thoải mái mà không béo, ngược lại, có những món ăn chỉ với 1 lượng nhỏ thôi cũng cung cấp quá nhiều năng lượng. Bảng so sánh dưới đây sẽ giúp bạn lựa chọn thực phẩm một cách thông minh hơn: Với mức cơ bản là 200 calo, hãy xem thực phẩm nào chứa nhiều và ít calo để có sự lựa chọn phù hợp nhé!', 'public/images/thumbnails/thumbnail4.jpg', '2016-01-23'),
(5, 2, 'Cười giúp giảm cân tốt hơn ăn kiêng và tập thể dục', 'Tiếng cười giúp cơ thể đốt đến 1.000 calo mỗi ngày và bảo vệ bạn khỏi các vấn đề như stress hay bệnh tim mạch.\r\nHãy bỏ qua các chế độ ăn kiêng và tập luyện hà khắc vì phương pháp tốt nhất để giảm cân là cười thật lớn, Metro đưa tin. Nhờ việc cười, bạn có thể đốt 100-120 calo trong một tiếng, tương ứng với 1.000 cao mỗi ngày.\r\n\r\nTừ trước đến nay, tiếng cười được công nhận là có nhiều tác động tích cực đến cơ thể con người như làm tăng lượng endorphin trong não, giảm căng thăng đau đớn. Ngoài ra, các nhà khoa học chứng minh cười tốt cho hệ thống tim mạch và giúp vùng bụng săn chắc. Một nghiên cứu năm 2014 chỉ ra cười sảng khoái kích hoạt cơ bụng hiệu quả hơn tập gập bụng.', 'public/images/thumbnails/thumbnail5.jpg', '2016-01-20'),
(6, 1, 'Tiến Đạt không muốn chuyện chia tay Hari Won bị bàn tán', 'Khuya 18/1, sau khi ra sân bay tiễn Hari Won về Hàn Quốc, Tiến Đạt mới chia sẻ về chuyện của anh và cô trên trang cá nhân. Cả ngày hôm qua, Tiến Đạt giữ im lặng, tuy nhiên trước việc Hari bị nhiều người trách móc, chàng rapper quyết định lên tiếng.\r\n\r\nTheo anh, chuyện gì đến sẽ đến, lỗi là do anh không giữ được lời hứa của mình. Ban đầu, anh tưởng việc này rất đơn giản, nhưng thực sự nó lại không hề đơn giản như anh nghĩ. Chính điều đó đã làm Tiến Đạt - Hari hay buồn và tranh cãi. Tiến Đạt nghĩ rằng anh không thể ích kỷ khi thấy người mình thương phải buồn rầu vì anh, vì lời hứa đối với anh thì đơn giản nhưng lại rất quan trọng với cô. Càng sống với nhau lâu, Tiến Đạt tự cảm thấy mình không làm được việc đó, không giữ được lời hứa của mình. "Chúng tôi phải xa nhau thôi, vì lỗi là tôi mà". \r\nTiến Đạt nói, tình yêu của anh và Hari dành cho nhau 9 năm qua là sự thật. Nó rất đẹp và họ trân trọng tình yêu đó. Song, cả hai đã nhìn ra vấn đề và rất khó khăn mới đưa ra được quyết định như vậy. Tiến Đạt không nói rõ lời hứa đó là gì và anh đã mắc sai lầm gì với Hari. \r\n\r\nThông qua "tâm thư", Tiến Đạt mong mọi người đừng lên án hay phán xét Hari, vì cô đã chịu nhiều khổ cực, đắng cay. Anh kể, từ nhỏ Hari đã bị kỳ thị, không hạnh phúc, ấm êm. Khi lớn lên, cô cũng khổ cực chứ không như những gì mọi người thấy trên truyền hình hay truyền thông. Dù cô có kể, mọi người cũng chỉ có thể cảm nhận được một phần chứ không thể biết hết được. Với riêng Tiến Đạt, anh cảm nhận được nỗi khổ của Hari nên rất yêu và tôn trọng cô. \r\n\r\nĐiều mà Tiến Đạt muốn là Hari sẽ luôn mỉm cười, sẽ tìm được một người thích hợp hơn và hiểu cô. Anh không thể ích kỷ giữ lại để làm khổ cô. "Tình cảm Hari và Tiến Đạt dành cho nhau là chân thật, nên các bạn đừng đoán già đoán non làm gì. Đừng đem lời cay nghiệt ra để miệt thị Hari, đừng trách Hari của Tiến Đạt. Hari xứng đáng nhận được tình thương yêu nhiều hơn. Các bạn giúp Tiến Đạt đừng để Hari phải chịu thêm bất cứ nỗi đau nào nữa nhé. Hãy gửi đến Hari của tôi những lời tốt đẹp nhất và hãy cầu nguyện cho Hari luôn hạnh phúc, vượt qua mọi khó khăn trong cuộc sống". \r\n\r\nPhần cuối bức thư, Tiến Đạt viết riêng cho Hari. Anh gọi cô là "baza" (bà già) và xưng là "ongza" (ông già). Tiến Đạt nói: "Anh biết tình mình chỉ đến đây thôi. Lỗi này là do anh tự chuốc lấy, khi chia ly rồi ai cũng buồn cũng đau, lòng sẽ hụt hẫng lắm. Những lúc thế này lòng yếu mềm. Có người chúc, người buồn, kẻ cơ hội. Cầu mong em tỉnh táo để bước đi. Anh vẫn sẽ luôn bên cạnh. Dù tình mình hết nhưng nghĩa luôn còn em nhé. Anh là người thân của em".', 'public/images/thumbnails/thumbnail6.jpg', '2016-01-19'),
(7, 3, 'Tại sao con ngươi của mèo thẳng đứng?', 'Con ngươi (Đồng tử) là lỗ nhỏ tròn giữa tròng đen con mắt.\r\nTuy nhiên, con ngươi của một số động vật lại biến dạng thành một đường thẳng đứng hoặc dọc. Tại sao mắt chúng lại đặc biệt như vậy?\r\nTheo nghiên cứu mới cho thấy, đó là do việc chúng sử dụng tầm nhìn của mình.\r\nSau khi tiến hành phân tích 214 loài động vật khác nhau, các nhà nghiên cứu đến từ đại học UC Berkeley (Mỹ) đã phát hiện ra hình dạng con ngươi của mèo thay đổi do tập tính hoạt động ban đêm của chúng.\r\nNhóm nghiên cứu đã công bố công trình nghiên cứu này của mình vào ngày 7/8/2015 trên tạp chí Science Advances.\r\nTheo công bố, hình dạng và kích cỡ con ngươi của mèo cho biết chúng cảm thụ ánh sáng như thế nào. Sau đó, não tiếp nhận và xử lý hình ảnh mà chúng thấy.\r\nCon ngươi trong mắt mèo có thể mở to tròn hết mức và cũng có thể co lại thành một đường thẳng đứng. Khi trời tối, đồng tử trong mắt mèo mở rộng để chúng có thể tiếp nhận ánh sáng tối đa và nâng cao tầm nhìn.\r\nNhưng khi trời sáng, đồng tử của chúng lại khép lại để thích nghi với ánh sáng mạnh. Theo một báo cáo từ đại học UC Berkeley, khi đồng tử mèo co lại, dẫn đến một sự thay đổi lớn giữa các trạng thái co lại và giãn ra.\r\nNó có thể làm tăng kích thước con ngươi từ 135 đến 300 lần. Trong khi, con ngươi của người chỉ tăng khoảng 15 lần.\r\nMèo là một loài ăn đêm và hoạt động mạnh nhất vào ban đêm. Do đó, sự biến dạng này mang lại cho chúng một lợi thế rất lớn khi đi săn.\r\n', 'public/images/thumbnails/thumbnail7.jpg', '2016-03-05'),
(8, 3, 'Nghiên cứu nói vào mùa thu bạn nhớ dai còn mùa hè lại dễ tập trung hơn', 'Bạn thường cảm thấy mùa đông u ám và mùa xuân thì tràn ngập niềm vui.\r\nNgoài cảm giác bình thường này thì một nghiên cứu mới đây vừa được trình bày trong kỷ yếu của Viện hàn lâm Nga còn cho biết các mùa cũng có thể thay đổi cách suy nghĩ của bạn.\r\nKhi so sánh các chức năng nhận thức của 28 tình nguyện viên được kiểm tra tại các thời điểm khác nhau trong năm, các nhà nghiên cứu nhận ra sự thay đổi rõ rệt trong vùng não của họ theo từng mùa.\r\n \r\n\r\n \r\nCụ thể, các khu vực có liên quan đến bộ nhớ làm việc có hiệu suất cao vào mùa thu, các khu vực liên quan đến khả năng tập trung lại hoạt động hiệu quả hơn vào mùa hè.\r\nMặc dù còn sớm để kết luận yếu tố mùa trong năm có tác động lên tinh thần của con người nhưng ít nhất nghiên cứu này cũng đã bước đầu cho thấy có mối quan hệ giữa các thời điểm trong năm với kết quả học tập và hành vi của họ.\r\nPierre MAQUET và Gilles Vandewalle, hai nhà khoa học dẫn đầu cuộc nghiên cứu thuộc đại học Liège (Bỉ) đã tuyển 28 tình nguyện viên khỏe mạnh, chia đều theo giới tính và có độ tuổi khoảng 21.\r\nĐể loại trừ ảnh hưởng của nhịp điệu hàng ngày và các yếu tố môi trường, các nhà nghiên cứu chuẩn bị điều kiện để các tình nguyện viên ở trong phòng thí nghiệm khoảng 4,5 ngày.\r\nTrong thời gian này, những người tham gia phải chịu đựng cảm giác thiếu ngủ 42 giờ trong một căn phòng cách âm, ánh sáng mờ và hoàn toàn không có khái niệm về thời gian.\r\nSau đó họ có một giấc ngủ ngon để phục hồi trước khi bước vào các thử nghiệm về nhận thức, hoàn thành các nhiệm vụ đòi hỏi phải duy trì sự chú tâm cao.\r\n', 'public/images/thumbnails/thumbnail8.jpg', '2016-03-06'),
(9, 3, 'Nhật thực sắp xuất hiện ở Việt Nam', 'Theo Timeanddate.com, người dân ở Indonesia, khu vực miền Trung và Đông Thái Bình Dương có cơ hội ngắm nhật thực toàn phần vào ngày 9/3.\r\nCác nước Đông Nam Á, trong đó có Việt Nam, và Australia sẽ quan sát được hiện tượng nhật thực một phần.\r\n \r\n\r\n \r\nTại Indonesia, nhật thực toàn phần dự kiến bắt đầu từ 6h30 (giờ địa phương) và kéo dài trong vài phút.\r\nHiện tượng nhật thực xảy ra khi mặt trăng đi qua giữa trái đất và mặt trời. Nhật thực một phần xảy ra khi mặt trăng che khuất một phần của mặt trời.\r\nViệc quan sát nhật thực trực tiếp bằng mắt thường có thể gây tổn thương cho mắt. Các nhà khoa học khuyến cáo người quan sát nên sử dụng thiết bị hoặc dụng cụ khi chiêm ngưỡng hiện tượng thiên văn kỳ thú này.\r\nChia sẻ với Zing.vn, anh Đặng Vũ Tuấn Sơn, Chủ tịch Hội Thiên văn học trẻ Việt Nam, cho biết tại Hà Nội, hiện tượng che khuất sẽ bắt đầu từ khoảng 6h57 và kéo dài đến 8h39. Tại TP HCM, hiện tượng sẽ bắt đầu lúc 6h35 và kết thúc lúc 8h41. Tất cả các tỉnh thành trên cả nước có thể theo dõi nhật thực, nhưng tỷ lệ che khuất sẽ không giống nhau. Càng vào phía nam, tỷ lệ che khuất càng lớn. Theo anh Sơn, tỷ lệ che khuất ở TP HCM là khoảng 80%, trong khi tại Hà Nội chỉ khoảng 20%.\r\nĐể quan sát nhật thực, người yêu thiên văn có thể lựa chọn địa điểm là các toà nhà cao tầng và hướng về phía đông. Vì nhìn trực tiếp bằng mắt thường sẽ ảnh hưởng đến võng mạc, anh Sơn khuyến cáo người xem có thể đeo kính râm (nhìn dưới 30 giây) hoặc sử dụng các thiết bị chuyên dụng như kính quan sát mặt trời.\r\nHội Thiên văn Hà Nội dự kiến tổ chức quan sát nhật thực ngày 9/3 tại khu vực bán đảo Linh Đàm.\r\n', 'public/images/thumbnails/thumbnail9.jpg', '2016-03-07'),
(10, 2, 'Làm gì khi dầu mỡ sôi, dung dịch lau dọn bắn vào mắt?', 'Khi bị dầu, mỡ sôi, gia vị làm bếp hay các dung dịch lau dọn bếp bắn vào mắt bạn có thể bảo vệ đôi mắt ngay lập tức bằng những cách dưới đây:\r\n*Gia vị làm bếp bắn vào mắt:\r\nKhi bạn thao tác với hạt tiêu hay các gia vị cay nóng khác, chúng có thể bám vào các ngón tay và lọt vào mắt nếu chúng ta dùng tay quệt hay dụi vào mắt. Do vậy, bạn nên rửa tay kỹ hoặc đeo găng tay khi làm bếp. Nếu chẳng may hạt tiêu hay gia vị nóng chui vào mắt, bạn nên xối rửa bằng nhiều nước. Cũng nên rửa quanh mắt bằng dầu gội trẻ em. Không nên dùng xà phòng trực tiếp vào mắt.\r\n*Bị mỡ nóng bắn vào mắt:\r\nDầu hay mỡ nóng có thể nổ khi sôi và bắn vào mắt dễ dàng. Phòng hộ là cách tốt nhất. Nếu bạn không có kính bảo vệ khi đun nấu thì cũng nên trang bị khiên chắn hay nắp đậy cho chảo hay xoong nồi.\r\nNếu chẳng may bị dầu mỡ nóng bắn vào mắt bạn nên xối rửa bằng nhiều nước ngay lập tức. Động tác này sẽ giúp đẩy ra ngoài các tiểu thể dầu mỡ và chất bẩn khác. Nếu chấn thương mắt đã rõ ràng, đau nhiều, các khó chịu không giảm nhanh thì bạn nên đi khám bác sĩ mắt càng sớm càng tốt. Chấn thương nếu ở dạng vi thể thì các dung dịch nước mắt nhân tạo sẽ giúp bạn dễ chịu nhanh chóng. Đừng nên dùng các thuốc chống đỏ mắt, chúng sẽ làm mắt của bạn dễ bị nhiễm trùng hơn và quá trình hàn gắn vết thương lâu hơn.\r\n', 'public/images/thumbnails/thumbnail10.jpg', '2016-03-02');

-- --------------------------------------------------------

--
-- Table structure for table `topic`
--

CREATE TABLE IF NOT EXISTS `topic` (
  `id` int(255) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `topic`
--

INSERT INTO `topic` (`id`, `name`) VALUES
(1, 'star'),
(2, 'health'),
(3, 'entertain');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`) VALUES
(1, 'admin1', '7c4a8d09ca3762af61e59520943dc26494f8941b'),
(2, 'admin2', '7c4a8d09ca3762af61e59520943dc26494f8941b');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`id`),
  ADD KEY `topic_id` (`topic_id`);

--
-- Indexes for table `topic`
--
ALTER TABLE `topic`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `article`
--
ALTER TABLE `article`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `topic`
--
ALTER TABLE `topic`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `article`
--
ALTER TABLE `article`
  ADD CONSTRAINT `article_ibfk_1` FOREIGN KEY (`topic_id`) REFERENCES `topic` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
